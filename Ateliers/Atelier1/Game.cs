using System;

namespace Ateliers
{
    class Game
    {
        public int wrongdoor;
        public bool[] doors = new bool[3];

        public void resetValues() {
            Array.Clear(doors, 0, doors.Length);
            wrongdoor = 0;
        }

        public void automaticPlay(int numberOfGame, bool withChange) {
            int win = 0;
            for (int i = 0; i < numberOfGame; i++)
            {
                resetValues();
                Random random = new Random();
                int randomNumber = random.Next(0, 3);
                doors[randomNumber] = true;

                int resp = random.Next(0, 3);

                for (int j = 0; j < doors.Length; j++)
                {
                    if (doors[j] == false && j != resp)
                    {
                        wrongdoor = j;
                        break;
                    }
                }

                if (withChange == true)
                {
                    resp = random.Next(0, 3);
                }

                if (doors[resp] == true){
                    win += 1;
                }

            }
            double proba = ((double)win / (double)numberOfGame) * 100;
            Console.WriteLine("Change:" + withChange.ToString());
            Console.WriteLine("Nombre de victoire sur " + numberOfGame.ToString() + " Parties : " + win.ToString());
            Console.WriteLine("Probabilité de gagner:" + proba.ToString());

        }

        public void manualPlay()
        {
            resetValues();
            Random random = new Random();
            int randomNumber = random.Next(0, 3);
            doors[randomNumber] = true;

            Console.WriteLine("Chose a door (select between 1 and 3)");
            String response = Console.ReadLine();
            int resp = Int32.Parse(response);
            if (resp >= 1 && resp <= 3)
            {
                for (int i = 0; i < doors.Length; i++)
                {
                    if (doors[i] == false && i != resp - 1)
                    {
                        wrongdoor = i + 1;
                        break;
                    }
                }
                Console.WriteLine("One of the wrong doors is: " + wrongdoor.ToString());
                Console.WriteLine("Would you like to change ? (y/n)");
                String next = Console.ReadLine();

                if (next == "y")
                {
                    Console.WriteLine("Chose a door (select between 1 and 3)");
                    response = Console.ReadLine();
                    resp = Int32.Parse(response);
                }

                Console.WriteLine("The value behind the door is :" + doors[resp - 1].ToString());
            }

        }
    }
}