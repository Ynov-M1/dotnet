﻿using System;
using Ateliers;

namespace Ateliers
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.automaticPlay(10000, false);
            game.automaticPlay(10000, true);
        }
    }
}
