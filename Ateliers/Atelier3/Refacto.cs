// To be refacto
public static double Calculate(int type, int choice, int value)
{
	double result = 0;
	if (type == 1)
	{
		switch (choice)
		{
			case 1: result = 2 * Math.PI * value; break;
			case 2: result = Math.PI * Math.Pow(value, 2); break;
		}
	}
	else if (type == 2)
	{
		switch (choice)
		{
			case 1: result = 3 * value; break;
			case 2: result = (Math.Sqrt(3) / 4) * Math.Pow(value, 2); break;
		}
	}
	else if (type == 3)
	{
		switch (choice)
		{
			case 1: result = 4 * value; break;
			case 2: result = Math.Pow(value, 2); break;
		}
	}

	return result;
}

//Actual refacto

public enum GeometricForm {
	Circle,
	Triangle,
	Square
}

public static double CalculatePerimeter(GeometricForm form, int cote) {
	double result = 0;
	switch (form):
		case form.Circle:
			result = 2 * Math.PI * cote; break;
		case form.Triangle:
			result = 3 * cote; break;
		case form.Square:
			result = 4 * cote; break;

	return result
}

public static double CalculateArea(GeometricForm form, int cote) {
	double result = 0;
	switch (form):
		case form.Circle:
			result = Math.PI * Math.Pow(cote, 2); break;
		case form.Triangle:
			result = (Math.Sqrt(3) / 4) * Math.Pow(cote, 2); break;
		case form.Square:
			result = Math.Pow(cote, 2); break;

	return result
}