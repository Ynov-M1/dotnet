using System;

namespace Atelier2
{
    abstract class DiabloClass
    {
        protected int Strength;
        protected int Dexterity;
        protected int Intelligence;
        protected int Vitality;
        protected CombatMethod CombatMethod;
        protected Role Role;

        public abstract string ClassType
        {
            get;
        }

        public abstract bool isAllowed(Role Role);

        public DiabloClass(){}
        public DiabloClass(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role){

            if (isAllowed(Role) == false)
            {
                throw new ArgumentException("role is invalid");
            }
            this.Strength = Strength;
            this.Dexterity = Dexterity;
            this.Intelligence = Intelligence;
            this.Vitality = Vitality;
            this.CombatMethod = combatMethod;
            this.Role = Role;
        }

        public virtual string Status
        {
            get { return $"{ClassType} - {Role} - {CombatMethod} - {Strength} Str - {Dexterity} Dex - {Intelligence} Int - {Vitality} Vit"; }
        }

        public static DiabloClass Parse(String str) {
            String[] result = str.Split('|', str.Length);
            String ClassType = result[0];
            String Role = result[1];
            int Strength = Int32.Parse(result[2]);
            int Dexterity = Int32.Parse(result[3]);
            int Intelligence = Int32.Parse(result[4]);
            int Vitality = Int32.Parse(result[5]);

            Enum.TryParse(Role, out Role myRole);

            switch (ClassType)
            {
                case "Barbarian":
                    return new Barbarian(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]));
                case "Crusader":
                    return new Crusader(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]));
                case "Demon Hunter":
                    return new DemonHunter(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]), Int32.Parse(result[7]));
                case "Monk":
                    return new Monk(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]));
                case "Necromancer":
                    return new Necromancer(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]));
                case "Witch Doctor":
                    return new WitchDoctor(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]));
                case "Wizard":
                    return new Wizard(Strength, Dexterity, Intelligence, Vitality, CombatMethod.Melee, myRole, Int32.Parse(result[6]));

                default:
                 throw new ArgumentException("Wrong string");
            }
        }
    }
}
