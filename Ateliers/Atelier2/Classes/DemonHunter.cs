using System;

namespace Atelier2
{
    class DemonHunter : DiabloClass
    {
        private int Hatred;
        private int Discipline;

        public const string Type = "DemonHunter";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Hatred} Hatred - {Discipline} Discipline"; }
        }

        public DemonHunter(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Hatred, int Discipline)
        : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Hatred = Hatred;
            this.Discipline = Discipline;
        }

        public override bool isAllowed(Role Role)
        {
            if (Role == Role.DPS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
