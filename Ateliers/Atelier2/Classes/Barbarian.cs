using System;

namespace Atelier2
{
    class Barbarian : DiabloClass
    {
       private int Fury;
        public const string Type = "Barbarian";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Fury} Fury"; }
        }

       public Barbarian(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Fury)
       : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Fury = Fury;
        }

        public override bool isAllowed(Role Role){
            if(Role == Role.DPS || Role == Role.TANK){
                return true;
            } else {
                return false;
            }
        }
    }
}
