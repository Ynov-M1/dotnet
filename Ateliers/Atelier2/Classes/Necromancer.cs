using System;

namespace Atelier2
{
    class Necromancer : DiabloClass
    {
        private int Essence;

        public const string Type = "Necromancer";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Essence} Essence"; }
        }
        public Necromancer(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Essence)
        : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Essence = Essence;
        }

        public override bool isAllowed(Role Role)
        {
            if (Role == Role.DPS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
