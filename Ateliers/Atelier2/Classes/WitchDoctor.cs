using System;

namespace Atelier2
{
    class WitchDoctor : DiabloClass
    {
        private int Mana;

        public const string Type = "Witch Doctor";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Mana} Wrath"; }
        }

        public WitchDoctor(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Mana)
        : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Mana = Mana;
        }

        public override bool isAllowed(Role Role)
        {
            if (Role == Role.CASTER || Role == Role.SUPPORT)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
