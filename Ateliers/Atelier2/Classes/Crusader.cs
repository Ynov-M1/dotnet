using System;

namespace Atelier2
{
    sealed class Crusader : DiabloClass
    {
        private int Wrath;

        public const string Type = "Crusader";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Wrath} Wrath"; }
        }
        public Crusader(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Wrath)
        : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Wrath = Wrath;
        }

        public override bool isAllowed(Role Role)
        {
            if (Role == Role.DPS || Role == Role.TANK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
