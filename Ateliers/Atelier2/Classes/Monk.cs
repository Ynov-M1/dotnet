using System;

namespace Atelier2
{
    class Monk : DiabloClass
    {
        private int Spirit;

        public const string Type = "Monk";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Spirit} Spirit"; }
        }

        public Monk(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Spirit)
        : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Spirit = Spirit;
        }

        public override bool isAllowed(Role Role)
        {
            if (Role == Role.DPS || Role == Role.TANK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
