using System;

namespace Atelier2
{
    class Wizard : DiabloClass
    {
        private int Arcane;

        public const string Type = "Wizard";

        public override string ClassType
        {
            get { return Type; }
        }
        public override string Status
        {
            get { return $"{base.Status} - {Arcane} Arcane"; }
        }
        public Wizard(int Strength, int Dexterity, int Intelligence, int Vitality, CombatMethod combatMethod, Role Role, int Arcane)
        : base(Strength, Dexterity, Intelligence, Vitality, combatMethod, Role)
        {
            this.Arcane = Arcane;
        }
        public override bool isAllowed(Role Role)
        {
            if (Role == Role.DPS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
